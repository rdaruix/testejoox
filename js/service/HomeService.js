angular.module('Joox').factory('Home', function($http, $window){
    
    return {
        listar: function(){
            return $http.get('http://preview-admin-api.joox.io/v1/stores/search/',{},{'headers':{'Ahtorization':$window.sessionStorage.token}});
        }
    };
})