var jwt = require('express-jwt');
var tokenManager  = require('../../config/token_manager.js');
var secret = require('../../config/secret.js');

module.exports = function(app){
    
    var controller = app.controllers.login;

    app.get('/logout', jwt({secret: secret.secretToken}), controller.logout);
    
}
