var app =  angular.module('Joox', ['ngRoute', 'ngResource']);

app.config(function($routeProvider, $locationProvider){    
    $routeProvider.when('/', {
        templateUrl:'partials/login.html',
        controller:'LoginController'
    });
    
    $routeProvider.when('/home', {
        templateUrl:'partials/home.html',
        controller:'HomeController',
        access: { requiredAuthentication: true }
    }); 
    $routeProvider.otherwise({redirectTo:'/'});    
    
    $locationProvider.hashPrefix('');
});

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
});

app.run(function($rootScope, $location, $window, AuthenticationService, $resource) {
    $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute){
        
        if($window.sessionStorage.user != null){
            $rootScope.usuarioLogado = $window.sessionStorage.user;
        }
        if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication 
            && !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {
            $rootScope.message = "Você precisa estar logado!";
            $location.path("/login");
        }
    });
});
