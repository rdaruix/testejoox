angular.module('Joox').factory('Login', function($http){
    return {
        logIn: function(email, password) {
            return $http.post('http://preview-admin-api.joox.io/v1/account/login', {email: email, password: password});
        },

        logOut: function() {
            return $http.get('/logout');
        }
    }
});
