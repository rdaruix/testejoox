var jwt = require('jsonwebtoken');
var secret = require('../../config/secret.js');
var tokenManager  = require('../../config/token_manager.js');
var bcrypt = require('bcryptjs');
var md5 = require('crypto-md5');


module.exports = function(app){

    var controller = {};
      
    controller.getUsuarioLogado = function(req, res){
        if(req.user){
            return res.render('index', {usuarioLogado: req.user.loginUsuario});
        }
    };

    controller.logout = function(req, res) {
        if (req.user) {
            tokenManager.expireToken(req.headers.authorization);
            delete req.user;	
            return res.sendStatus(200);
        }
        else {
            return res.sendStatus(401);
        }
    };
    return controller;
};
