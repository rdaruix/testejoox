angular.module('Joox').controller('HomeController', ['$scope', '$window', 'Home', function($scope, $window, Home){
    
    Home.listar().then(function(data){
        $scope.lojas = data.data.data;     
    });
}]);