angular.module('Joox').factory('AuthenticationService', function() {  
    var auth = {
        isAuthenticated: false,
        isAdmin: false
    }

    return auth;
});