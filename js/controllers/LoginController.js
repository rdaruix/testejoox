angular.module('Joox').controller('LoginController', ['$scope', '$location', '$window', 'Login', 'AuthenticationService', '$rootScope',
    function($scope, $location, $window, Login, AuthenticationService, $rootScope){

        $rootScope.usuarioLogado = '';
        $scope.logIn = function logIn(email, password) {
            if (email != null && password != null) {
                Login.logIn(email, password).then(function(data){         
                    AuthenticationService.isAuthenticated = true;
                    $window.sessionStorage.token = data.data.data.authToken;
                    $window.sessionStorage.user = email;
                    $location.path("/home");
                },function(status) {
                    $rootScope.message = status.data.errorMsg;
                });
            }
        }

        $scope.logOut = function logOut() {
            if (AuthenticationService.isAuthenticated) {
                Login.logOut().then(function(data) {
                    AuthenticationService.isAuthenticated = false;
                    delete $window.sessionStorage.token;
                    delete $window.sessionStorage.user;
                    $rootScope.usuarioLogado = '';
                    $location.path("/");
                }, function(status, data) {
                    console.log(status);
                    console.log(data);
                });
            }
            else {
                $location.path("/login");
            }
        }
}]);
