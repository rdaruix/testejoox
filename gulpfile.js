var gulp = require('gulp'),
    sass = require('gulp-sass'),
    htmlReplace = require('gulp-html-replace'),
    autoPrefixer = require('gulp-autoprefixer'),
    cssMin = require('gulp-cssmin'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    useMin = require('gulp-usemin'),
    cleanCSS = require('gulp-clean-css');

var paths = {
    sassSrcPath: ['./sass/**/*.scss'],
    sassDestPath: './css/'
};

var autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};


gulp.task('sass', function () {
    gulp.src(paths.sassSrcPath)
        .pipe(sass())
        .pipe(autoPrefixer(autoprefixerOptions))
        .pipe(gulp.dest(paths.sassDestPath));
});

gulp.task('minify',['sass'], function(){
  gulp.src(paths.sassDestPath+'**/*.css')
    .pipe(cleanCSS({compatibility:'ie8'}))
    .pipe(gulp.dest('./css'));
});

gulp.task('css',['minify'], function(){
   return gulp.src('./**/*.html')
    .pipe(htmlReplace({
       'css':'css/style.css',
       'elephant':'css/elephant.css',
       'application':'css/application.css',
       'vendor':'css/vendor.css'
   }));
});


gulp.task('watch', function() {
    gulp.watch(paths.sassSrcPath, ['sass']);
});

gulp.task('default', ['sass','minify','css','watch']);
