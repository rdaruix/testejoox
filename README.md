# Teste Joox#

Olá meu nome é Ricardo Salim. Esse é o resultado do teste solicitado

### Como rodar o teste? ###

* Fazer o clone desse repositório
* Acessar a pasta do projeto
* Executar o comando **npm install**
* Executar o comando **node index**
* Deverá aparecer uma mensagem *Express Server escutando na port 21018* 
* Abrir o navegador e acessar o link **localhost:21018**