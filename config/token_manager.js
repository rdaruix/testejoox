var app = require('../server/express.js')();
var express = require('express');

var TOKEN_EXPIRATION = 60;
var TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION * 60;



exports.verifyToken = function (req, res, next) {
    var token = req.body.token || req.query.token || req.headers['x-access-token'];

    if(token){
        jwt.verify(token, app.get('superSecret'), function(err, decoded) {
          if (err) {
            return res.json({ success: false, message: 'Failed to authenticate token.' });
          } else {
            req.decoded = decoded;
            next();
          }
        });
    }else{
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });
    }
};

exports.expireToken = function(headers) {
	var token = getToken(headers);
	if (token != null) {
		app.set(token, { is_expired: true });
	}
};

var getToken = function(headers) {
	if (headers) {
		var authorization = headers;
		var part = authorization.split(' ');

		if (part.length == 2) {
			var token = part[1];

			return part[1];
		}
		else {
			return null;
		}
	}
	else {
		return null;
	}
};


exports.TOKEN_EXPIRATION = TOKEN_EXPIRATION;
exports.TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION_SEC;
